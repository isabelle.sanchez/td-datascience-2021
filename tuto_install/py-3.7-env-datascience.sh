# un environnement python 3.7, contenant keras, tensoflow et scikit-learn
# configurations de channels pour accéder aux paquets à installer dans anaconda
conda config --add channels conda-forge
conda config --add channels anaconda

# creation de l'environnement
conda create -n py-3.7-env-datascience python=3.7
conda activate py-3.7-env-datascience

conda install -c anaconda -y jupyter

# Remplissage de l'environnement avec quelques packages python
conda install -c conda-forge matplotlib
conda install -c conda-forge numpy
conda install -c anaconda pandas
conda install -c conda-forge seaborn

# Remplissage de l'environnement avec les packages de machineL
conda install -c anaconda scikit-learn 

# Remplissage de l'environnement avec les packages de deepL
conda install -c anaconda tensorflow
pip install tensorflow --upgrade
conda install -c conda-forge keras

# pour désactiver votre environnement en fin de travail
# conda deactivate
